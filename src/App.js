import React from 'react';
import AddFriend from './components/AddFriend';
import Friend from './components/Friend';
import uuid from 'uuid/v1';
import EditFriend from './components/EditFriend';

class App extends React.Component{

  state = {
    friends : [
    {
      name:"Kenji",
      age: 33,
      id: uuid()
    },
    {
      name:"Ottomo",
      age: 46,
      id: uuid()
    }
    ]
  }

    handleAddFriend=(friend)=>{
      friend.id=Math.random()
      this.setState({friends:[
        ...this.state.friends,
        friend
        ]})
    }

    handleEditFriend=(id,updatedFriend)=>{
    //   this.setState({friends.map(friend=>{
    //     return friend.id==id?updatedFriend:friend
    //   })
    // })
    let updatedFriends=this.state.friends.map(friend=>{
      return friend.id==id?updatedFriend:friend
    });
    this.setState({friends:updatedFriends})
    console.log(updatedFriends)
    }

    handleDeleteFriend=(id)=>{
      // filter
      let updatedFriends=this.state.friends.filter(friend=>{
        return friend.id !== id
      })
      this.setState({
        friends:updatedFriends
      })
    }

   

  render(){
    return(
      <React.Fragment>
        <h1>
          "Hello Universe"
        </h1>
        <AddFriend addFriend={this.handleAddFriend}/>
        <Friend
          friends={this.state.friends}
          deleteFriend={this.handleDeleteFriend}/>
        <EditFriend editFriend={this.state.friends} editPeer={this.handleEditFriend}/>  
      </React.Fragment>
    )
  }
}

export default App;



// // import React, {useState} from 'react';
// // import Friend from './component/Friend';
// // function App(){
// //   const[friends,setFriends]=useState([
// //   {
// //     name:"Alex",
// //     age: 14,
// //     id: Math.random()
// //   },
// //   {
// //     name:"Alex",
// //     age: 14,
// //     id: Math.random()
// //   }
// //   ])

// // const addFriend=friend=>{
// //   setFriends([
// //     ...friends,
// //     {name:friend.name,age:friend.age,id:Math.random()}
// //   ])
// // }


// // }

// // return(
// //   <React.Fragment>
// //         <h1>
// //           "Hello Universe"
// //         </h1>
//           <Friend friends={friends}/>
// //       </React.Fragment>
// // )