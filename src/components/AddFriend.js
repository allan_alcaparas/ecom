import React from 'react';

class AddFriend extends React.Component {

	state = {	
			name: null,
			age: null
	}

	handleChange=(e)=>{
		console.log(e.target.id)
		this.setState({[e.target.id] : e.target.value})
	}

	handleSubmit=(e)=>{
		e.preventDefault()
		// console.log(this.props)
		this.props.addFriend(this.state)
		this.setState({name:null,age:null})
		e.target.reset()
	}	
	

	// handleClick=(e)=>{
	// 	console.log('Clicked!')
	// }	

	render(){
		return(
			<div className='bg-dark text-white'>
				<h1>
					Form to add Friend
				</h1>
				<form action="" onSubmit={this.handleSubmit}>
					Name:
					<input onChange={this.handleChange} id='name' type='text'/>
					Age:
					<input onChange={this.handleChange} id='age' type='number'/>
					<button>
						Add Ninja
					</button>
				</form>
			</div>
		)
	}
}

export default AddFriend;


					// type='button' onClick={this.handleClick}>